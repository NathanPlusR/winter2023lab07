import java.util.Scanner;

public class Board
{	
	private Square[][] tictactoeBoard;
	
	public void createBoard()
	{
		this.tictactoeBoard = new Square[3][3];
		
		for (int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for (int j = 0; j < this.tictactoeBoard.length; j++)
			{
				this.tictactoeBoard[i][j] = Square.BLANK;
			}
		}
		
	}
	
	public void updateBoard(Square insert, int x, int y)
	{
		this.tictactoeBoard[x][y] = insert;
	}
	
	public void takeTurn(Square square)
	{
		System.out.println("It's " + square + "'s turn!");
		Scanner scan =  new Scanner(System.in);
		boolean invalidInput = true;
		
		while (invalidInput)
		{
			System.out.println("Enter the number of the square you want!");
			int squareID = scan.nextInt();
			
			for (int i = 0; i < this.tictactoeBoard.length; i++)
			{
				for (int j = 0; j < this.tictactoeBoard.length; j++)
				{
					if (squareID == (i*3)+(j+1))
					{
						if (this.tictactoeBoard[i][j] == Square.BLANK)
						{
							this.updateBoard(square, i, j);
							invalidInput = false;
						}
						else
						{
							System.out.println("That square is already in use! Try again!");
						}
					}				
				}
			}
		}
	}
	
	public Square whoWon()
	{	
		Square winner = Square.BLANK;
		//check for row
		for (int i = 0; i < this.tictactoeBoard.length; i++)
		{
			if (this.tictactoeBoard[i][0] == this.tictactoeBoard[i][1] && this.tictactoeBoard[i][1] == this.tictactoeBoard[i][2])
			{
				if (this.tictactoeBoard[i][0] == Square.X)
				{
					winner = Square.X;
				}
				else if (this.tictactoeBoard[i][0] == Square.O)
				{
					winner = Square.O;
				}
			}
		}
		//check for column
		for (int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for (int j = 0; j < this.tictactoeBoard.length; j++)
			{
				if (this.tictactoeBoard[0][j] == this.tictactoeBoard[1][j] && this.tictactoeBoard[1][j] == this.tictactoeBoard[2][j])
				{
					if (this.tictactoeBoard[0][j] == Square.X)
					{
						winner = Square.X;
					}
					else if (this.tictactoeBoard[0][j] == Square.O)
					{
						winner = Square.O;
					}
				}
			}
		}
		//check diagonals
		if (this.tictactoeBoard[0][0] == this.tictactoeBoard[1][1] && this.tictactoeBoard[1][1] == this.tictactoeBoard[2][2])
		{
			if (this.tictactoeBoard[0][0] == Square.X)
			{
				winner = Square.X;
			}
			else if (this.tictactoeBoard[0][0] == Square.O)
			{
				winner = Square.O;
			}
		}
		
		if (this.tictactoeBoard[0][2] == this.tictactoeBoard[1][1] && this.tictactoeBoard[1][1] == this.tictactoeBoard[2][0])
		{
			if (this.tictactoeBoard[0][2] == Square.X)
			{
				winner = Square.X;
			}
			else if (this.tictactoeBoard[0][2] == Square.O)
			{
				winner = Square.O;
			}
		}
		
		return winner;
	}
	
	public boolean tieCheck()
	{
		int blankCount = 0;
		
		for (int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for (int j = 0; j < this.tictactoeBoard.length; j++)
			{
				if (this.tictactoeBoard[i][j] == Square.BLANK)
				{
					blankCount += 1;
				}
			}
		}
		
		if (blankCount == 0)
		{
			return false;
		}
		else
		{
			return true;
		}		
	}
	
	public String toString()
	{
		String boardDisplay = "";
		
		for (int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for (int j = 0; j < this.tictactoeBoard.length; j++)
			{
				if (this.tictactoeBoard[i][j] != Square.BLANK)
				{
					boardDisplay = boardDisplay + this.tictactoeBoard[i][j] + " ";
				}
				else
				{
					boardDisplay = boardDisplay + ((i*3)+(j+1)) + " ";
				}
			}
			boardDisplay = boardDisplay + "\n";
		}
		
		return boardDisplay;
	}
	
	/* public String toString()
	{
		String boardDisplay = "";
		
		for (int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for (int j = 0; j < this.tictactoeBoard.length; j++)
			{
				boardDisplay = boardDisplay + this.tictactoeBoard[i][j] + " ";
			}
			boardDisplay = boardDisplay + "\n";
		}
		
		return boardDisplay;
	} */
}