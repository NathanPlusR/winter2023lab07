import java.util.Scanner;

public class Game
{	
	public static void main (String[] args)
	{
		boolean playing = true;
		Scanner scan =  new Scanner(System.in);
		
		while (playing)
		{
			boolean inGame = true;
			Board board = new Board();
			board.createBoard();
			
			System.out.println("Let's play!");
			
			Square x =  Square.X;
			Square o =  Square.O;
			Square turn = Square.O;
			
			while (inGame)
			{
				System.out.println(board);
				
				if (board.tieCheck())
				{
					if (board.whoWon() == Square.BLANK)
					{
						board.takeTurn(turn);
						
						if (turn == Square.O)
						{
							turn = Square.X;
						}
						else
						{
							turn = Square.O;
						}
					}
					else
					{
						System.out.println("\n" + board.whoWon() + " won! Congrats!");
						inGame = false;
					}
				}
				else 
				{
					System.out.println("It was a tie!");
					inGame = false;
				}
			}
			
			System.out.println("Do you want to play again? Type 0 if you want to stop and 1 to continue");
			int answer = scan.nextInt();
			
			if (answer == 0)
			{
				playing = false;
			}
		}
	}
}